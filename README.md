# Semantic Release CI task

1. Create an access token with the `api` scope in your project
2. Set this token in an environment variable `GITLAB_TOKEN`
3. Set another environment variable `GITLAB_URL` with `https://gitlab.vgiscience.de`
4. Use this YAML snippet in your project's `.gitlab-ci.yml` to create a semantic release task:

```yaml
release:
  image: registry.gitlab.vgiscience.org/tools/semantic-release:latest
  stage: deploy
  script:
    - semantic-release --plugins "@semantic-release/gitlab" --plugins "@semantic-release/release-notes-generator"
  only:
    - master
```
