FROM node:current-slim

RUN set -ex; \
    \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        git \
        ca-certificates \
    ; \
    apt-get autoremove -y \
    ; \
    rm -rf /var/lib/apt/lists/*;

RUN npm update --global && \
    npm install --global \
        semantic-release \
        @semantic-release/gitlab \
        @semantic-release/release-notes-generator
